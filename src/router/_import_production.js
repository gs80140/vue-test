module.exports = file => () => {
  try {
    return import('@/views' + file + '.vue')
  } catch (e) {
    return import('@/views' + '/404' + '.vue')
  }
}
