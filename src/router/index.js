import Vue from 'vue'
import Router from 'vue-router'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
 * hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
 *                                if not set alwaysShow, only more than one route under the children
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
 **/
export const constantRouterMap = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/print',
    component: () => import('@/views/print/index'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    meta: { title: '首页', icon: 'form' },
    children: [
      {
        path: 'dashboard',
        name: 'tmeplate',
        component: () => import('@/views/dashboard/index'),
        meta: { title: '首页', icon: 'form' }
      }
    ]
  }
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

// [
// {
//   path: "/shops",
//   component: Layout,
//   meta: { title: "店铺", icon: "form" },
//   children: [
//     {
//       path: "index",
//       name: "shops",
//       component: Layout,
//       meta: { title: "店铺", icon: "form" }
//     }
//   ]
// },
// {
//   path: "/goods",
//   component: Layout,
//   meta: { title: "商品", icon: "form" },
//   children: [
//     {
//       path: "index",
//       name: "goods",
//       component: () => import("@/views/goods/index"),
//       meta: { title: "商品列表", icon: "form" }
//     },
//     {
//       path: "add",
//       name: "goods_add",
//       component: () => import("@/views/goods/add"),
//       meta: { title: "添加商品", icon: "form" }
//     }
//   ]
// },

// {
//   path: "/orders",
//   component: Layout,
//   meta: { title: "订单", icon: "form" },
//   children: [
//     {
//       path: "index",
//       name: "orders",
//       component: () => import("@/views/orders/index"),
//       meta: { title: "订单", icon: "form" }
//     }
//   ]
// },

// {
//   path: "/finance",
//   component: Layout,
//   meta: { title: "财务", icon: "form" },
//   children: [
//     {
//       path: "index",
//       name: "finance",
//       component: () => import("@/views/finance/index"),
//       meta: { title: "财务", icon: "form" }
//     }
//   ]
// },
// {
//   path: "/settings",
//   component: Layout,
//   meta: { title: "设置", icon: "form" },
//   children: [
//     {
//       path: "carousel",
//       name: "carousel",
//       component: () =>
//         import("@/views/settings/carousel/index"),
//       meta: { title: "首页幻灯片", icon: "form" }
//     },
//     {
//       path: "index",
//       name: "settings",
//       component: () => import("@/views/settings/index"),
//       meta: { title: "设置", icon: "form" }
//     }
//   ]
// },
// { path: "*", redirect: "/404", hidden: true }
// ];
