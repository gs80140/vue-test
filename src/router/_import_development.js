module.exports = file => {
  try {
    return require('@/views' + file + '.vue').default
  } catch (e) {
    return require('@/views' + '/404' + '.vue').default
  }
}
// require('@/views' + file + '.vue').default // vue-loader at least v13.0.0+
