import Layout from '../views/layout/Layout'
export const routerMap = {
    /**测试数据 临时页面 S*/
    shop: {
        path: '/shops/index',
        component: () =>
            import ("@/views/shops/index"),
        meta: { title: "店铺", icon: "form" }
    },
    '/system/goods/list': {
        path: '/goods/list',
        component: () =>
            import ("@/views/goods/index"),
        meta: { title: "商品列表", icon: "form" }
    },
    goodsAdd: {
        path: "/goods/add",
        component: () =>
            import ("@/views/goods/add"),
        meta: { title: "添加商品", icon: "form" }
    },
    carousel: {
        path: "/carousel/index",
        component: () =>
            import ("@/views/settings/carousel/index"),
        meta: { title: "幻灯片管理", icon: "form" }
    },
    /**测试数据 E*/
    // 订单列表
    orderList: {
        path: "/order/list",
        component: () =>
            import ("@/views/orders/index"),
        meta: { title: "订单列表", icon: "form" }
    },
    '/system/user/list': {
        path: "/system/user/list",
        component: () =>
            import ("@/views/system/user/list"),
        meta: { title: "用户列表", icon: "form" }
    },
    '/system/role/list': {
        path: "/system/role/list",
        component: () =>
            import ("@/views/system/role/list"),
        meta: { title: "角色管理", icon: "form" }
    },
    '/system/menu/list': {
        path: "/system/menu/list",
        component: () =>
            import ("@/views/system/menu/list"),
        meta: { title: "菜单管理", icon: "form" }
    },
    '/code': {
        path: "",
        component:Layout,
        children:[{
            path:"/code/index",
            meta: { title: "代码生成器", icon: "form" },
            component: () =>
            import ("@/views/code/index"),
            name:"生成代码"
        }]
       
    },
}

// orgData:后台根据角色返回的菜单格式
// rtMap:路由映射
export const routerMapHandle = (orgData, rtMap) => {
    let rs = [];
    orgData.forEach(d => {
        if (d.children && d.children.length) {
            let rObj = {};
            rObj.component = Layout;
            rObj.path = '';
            rObj.meta = { title: d.menu.name, icon: 'form' };
            rObj.children = [];
            d.children.forEach(v => {
                if (v.menu.url && rtMap[v.menu.url]) {
                    let obj = rtMap[v.menu.url];
                    if (v.menu.path) {
                        obj.path = v.menu.path;
                    }
                    obj = Object.assign(obj, { name: v.menu.name });
                    rObj.children.push(obj);
                }
            })
            rs.push(rObj);
        } else {
            if (d.menu.url && rtMap[d.menu.url]) {
                let rObj = {};
                let rtInfo = rtMap[d.menu.url];
                // rObj.component = Layout;
                // rObj.path = rtInfo.menu.url;
                // rObj.meta = rtInfo.menu.name;
                // rObj.children = [Object.assign(rtInfo, { name: d.menu.name })];
                rs.push(rtInfo);
            }
        }
    });
    rs.push({
        path: '/goods/add',
        component: () =>
            import ("@/views/goods/add"),
        meta: { title: "商品添加", icon: "form" }
    });
    // 404页放最后，否则之后添加的页面都会404
    rs.push({ path: "*", redirect: "/404", hidden: true });
    return rs;
}
