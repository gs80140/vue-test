export default {
  goods: {
    addGoods: 'add',
    picture: 'picture',
    localServer: 'Local Server',
    pleaseChoose: 'Please choose',
    unlimitedYear: 'Unlimited year',
    unlimitedMonth: 'Unlimited month',
    uploadPictures: 'Upload pictures',
    allElection: 'all election',
    exNetPic: 'extracting Network Pictures',
    inputPicLink: 'input picture link',
    picLink: 'picture link',
    conversion: 'conversion',
    operateSucccess: 'Successful operation'
  },
  common: {
    add: 'add',
    view: 'view',
    edit: 'edit',
    del: 'delete',
    delAll: 'deleteAll',
    delAllTip: 'Please select data!',
    confirm: 'confirm',
    cancel: 'cancel',
    delTip: 'Are you sure you want to delete this record?',
    search: 'search',
    operate: 'operate',
    tip: 'Tips',
    deleteTip: 'This operation will permanently delete this data. Do you want to continue?',
    audit: 'audit',
    adopt: 'adopt',
    notPass: 'notPass',
    operateSucccess: 'Successful operation'
  }
}
