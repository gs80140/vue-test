export default {
  goods: {
    addGoods: '상품 추가',
    picture: '그림',
    localServer: '로컬 서버',
    pleaseChoose: '선택',
    unlimitedYear: '연도 불문',
    unlimitedMonth: '달을 제한하지 않다',
    uploadPictures: '사진 업로드',
    allElection: '전선',
    exNetPic: '네트워크 그림 추출',
    inputPicLink: '그림 링크 입력',
    PicLink: '그림 링크',
    conversion: '전환',
    operateSucccess: '조작이 성공하다'
  },
  common: {
    add: '추가',
    view: '조사하다',
    edit: '편',
    del: '삭제',
    delAll: '대량 삭제',
    delAllTip: '데이터 선택！',
    confirm: '확정',
    cancel: '취소',
    delTip: '이 기록 삭제 여부 확인？',
    search: '수색',
    operate: '조작',
    tip: '힌트',
    deleteTip: '이 동작은 이 데이터를 영원히 삭제할 것입니다. 계속합니까?',
    audit: '심사',
    adopt: '통과하다',
    notPass: '불통과',
    operateSucccess: '조작이 성공하다'
  }
}
