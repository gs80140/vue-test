export default {
  goods: {
    addGoods: '添加商品',
    picture: '图片',
    localServer: '本地服务器',
    pleaseChoose: '请选择',
    unlimitedYear: '不限年份',
    unlimitedMonth: '不限月份',
    uploadPictures: '上传图片',
    allElection: '全选',
    exNetPic: '提取网络图片',
    inputPicLink: '输入图片链接',
    PicLink: '图片链接',
    conversion: '转化',
    operateSucccess: '操作成功'
  },
  common: {
    add: '添加',
    view: '查看',
    edit: '编辑',
    del: '删除',
    delAll: '批量删除',
    delAllTip: '请选择数据！',
    confirm: '确定',
    cancel: '取消',
    delTip: '确认是否删除该条记录？',
    search: '搜索',
    operate: '操作',
    tip: '提示',
    deleteTip: '此操作将永久删除该条数据, 是否继续?',
    audit: '审核',
    adopt: '通过',
    notPass: '不通过',
    operateSucccess: '操作成功'
  }
}
