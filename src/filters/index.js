// set function parseTime,formatTime to filter
export { parseTime, formatTime } from '@/utils'

function pluralize(time, label) {
  if (time === 1) {
    return time + label
  }
  return time + label + 's'
}

const timeAgo = (time) => {
  const between = Date.now() / 1000 - Number(time)
  if (between < 3600) {
    return pluralize(~~(between / 60), ' minute')
  } else if (between < 86400) {
    return pluralize(~~(between / 3600), ' hour')
  } else {
    return pluralize(~~(between / 86400), ' day')
  }
}

/* 数字 格式化*/
const numberFormatter = (num, digits) => {
  const si = [
    { value: 1E18, symbol: 'E' },
    { value: 1E15, symbol: 'P' },
    { value: 1E12, symbol: 'T' },
    { value: 1E9, symbol: 'G' },
    { value: 1E6, symbol: 'M' },
    { value: 1E3, symbol: 'k' }
  ]
  for (let i = 0; i < si.length; i++) {
    if (num >= si[i].value) {
      return (num / si[i].value + 0.1).toFixed(digits).replace(/\.0+$|(\.[0-9]*[1-9])0+$/, '$1') + si[i].symbol
    }
  }
  return num.toString()
}

const toThousandFilter = (num) => {
  return (+num || 0).toString().replace(/^-?\d+/g, m => m.replace(/(?=(?!\b)(\d{3})+$)/g, ','))
}

const orderStatus = (value) => {
  switch (value) {
    case 0:
      return '创建'
    case 1:
      return '已付款'
    case 2:
      return '已发货'
    case 3:
      return '待收货'
    case 4:
      return '已评价'
    case -1:
      return '退款'
  }
}

const auditStatus = (value) => {
  switch (value) {
    case 0:
      return '未提交'
    case 1:
      return '提交审核'
    case 2:
      return '审核通过'
    case 3:
      return '审核失败'
  }
}

export {
  timeAgo,
  numberFormatter,
  toThousandFilter,
  orderStatus,
  auditStatus
}
