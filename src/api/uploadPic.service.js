// 幻灯片相关接口
import request from '@/utils/request'

var api = {}

api.getImgArr = function getImgArr(params) {
  return request({
    url: '/service/basic/images/list',
    method: 'get',
    params
  })
}

api.delImg = function delImg(id) {
  return request({
    url: `/service/basic/images/delete/?id=${id}`,
    method: 'delete'
  })
}

export default api