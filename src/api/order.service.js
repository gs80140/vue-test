// 订单相关接口
import request from '@/utils/request'

var api = {}

api.add = function add(obj) {
  return request({
    url: '/service/basic/ads/add',
    method: 'put',
    data: obj
  })
}

api.del = function del(id) {
  return request({
    url: `/service/basic/ads/delete/?id=${id}`,
    method: 'delete'
  })
}

api.update = function update(obj) {
  return request({
    url: '/service/basic/ads/update',
    method: 'post',
    data: obj
  })
}

api.list = function list(obj) {
  return request({
    url: `/service/order/order/list`,
    method: 'get',
    params: obj
  })
}

api.orderDeliver = function list(obj) {
  return request({
    url: `/service/order/order-deliver/add`,
    method: 'put',
    data: obj
  })
}

// 快递公司列表接口
api.companyList = function list(obj) {
  return request({
    url: `/service/basic/express-company/list`,
    method: 'get',
    params: obj
  })
}

// 根据id查询订单详情
api.orderDetail = function list(obj) {
  return request({
    url: `/service/order/order/orderDetail`,
    method: 'get',
    params: obj
  })
}

// 修改订单备注
api.update = function update(obj) {
  return request({
    url: '/service/basic/ads/update',
    method: 'post',
    data: obj
  })
}

// 修改订单收货地址
api.updateOrderAddress = function update(obj) {
  return request({
    url: '/service/order/order/updateOrderAddress',
    method: 'post',
    data: obj
  })
}

// 修改订单备注
api.updateOrderItemRemark = function update(obj) {
  return request({
    url: '/service/order/order/updateOrderItemRemark',
    method: 'post',
    data: obj
  })
}

// 根据父id获取所有子区域
api.queryAreasByParentId = function list(obj) {
  return request({
    url: `/service/basic/areas/queryAreasByParentId`,
    method: 'get',
    params: obj
  })
}

// 根据订单id查询包裹号
api.getPackageNo = function list(obj) {
  return request({
    url: `/service/order/order-deliver/getPackageNo`,
    method: 'get',
    params: obj
  })
}

// 根据包裹号修改物流信息
api.updateDeliverInfo = function update(obj) {
  return request({
    url: '/service/order/order-deliver/updateDeliverInfo',
    method: 'post',
    data: obj
  })
}

export default api
