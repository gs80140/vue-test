// 幻灯片相关接口
import request from '@/utils/request'

var api = {}

api.getCateList = function getCateList() {
  return request({
    url: '/service/basic/goods-cats/goodsCats',
    method: 'GET'
  })
}
export default api