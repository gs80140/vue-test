import request from "@/utils/request";

// 角色列表
export function roleList(params){
    return request({
      url: `/service/system/role/list`,
      method: "GET",
      params: params
    })
  }
  
  // 删除角色
  export function deleteRole(params){
    return request({
      url: `/service/system/role/delete/${params}`,
      method: "DELETE"
    })
  }
  
  // 添加和更新角色
  export function addUpdateRole(params){
    return request({
      url: "/service/system/role/saveOrUpdate",
      method: "post",
      data: params
    })
  }
  
  //角色分配菜单
  export function roleGiveMent(params){
    return request({
      url: `/service/system/role/distribution`,
      method: "get",
      params: params
    })
  }
  //根据角色获取对应的菜单
  export function getRoleMenu(params){
    return request({
      url: `/service/system/role/role-menu-list`,
      method: "get",
      params: params
    })
  }