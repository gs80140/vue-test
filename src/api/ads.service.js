// 幻灯片相关接口
import request from '@/utils/request'

var api = {}

api.add = function add(obj) {
  return request({
    url: '/service/basic/ads/add',
    method: 'put',
    data: obj
  })
}

api.del = function del(params) {
  return request({
    url: `/service/basic/ads/delete`,
    method: 'delete',
    params
  })
}

api.update = function update(obj) {
  return request({
    url: '/service/basic/ads/update',
    method: 'post',
    data: obj
  })
}

api.list = function list(obj) {
  return request({
    url: `/service/basic/ads/list/`,
    method: 'get',
    params: obj
  })
}

export default api
