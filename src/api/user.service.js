import request from "@/utils/request";

// 用户列表
export function userList(params){
    return request({
      url: "/service/system/user/list",
      method: "GET",
      params: params,
    })
  }
  
  // 添加和编辑用户
  export function addUpdateUser(params){
    return request({
      url: "/service/system/user/saveOrUpdate",
      method: "post",
      data: params
    })
  }
  
  // 删除用户
  export function deleteUser(params){
    return request({
      url: `/service/system/user/delete/${params}`,
      method: "DELETE"
    })
  }
  
  //用户分配角色
  export function userRole(params){
    return request({
      url: `/service/system/user/distribution`,
      method: "PUT",
      params:params
    })
  }