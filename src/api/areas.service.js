// 幻灯片相关接口
import request from '@/utils/request'

var api = {}

api.add = function add(obj) {
  return request({
    url: '/service/basic/areas/add',
    method: 'put',
    data: obj
  })
}

api.del = function del(id) {
  return request({
    url: `/service/basic/areas/delete/?id=${id}`,
    method: 'delete'
  })
}

api.update = function update(obj) {
  return request({
    url: '/service/basic/areas/update',
    method: 'post',
    data: obj
  })
}

api.list = function list(obj) {
  return request({
    url: `/service/basic/areas/list`,
    params: obj,
    method: 'get'
  })
}

api.cityList = function cityList(obj){
  return request({
    url:'/service/basic/areas/queryAreasByParentId',
    method:'GET',
    params:obj
  })
}
api.proList = function proList(obj){
  return request({
    url:'/service/basic/areas/queryAreasByType',
    method:'GET',
    params:obj
  })
}

export default api
