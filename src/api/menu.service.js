import request from "@/utils/request";

// 菜单列表
export function menuList(params) {
    return request({
        url: "/service/system/menu/list",
        method: "get",
        params: params
    });
}


// 删除菜单
export function deletMenu(params) {
    return request({
        url: `/service/system/menu/delete/${params}`,
        method: "DELETE"
    })
}


// 添加以及修改菜单
export function addUpdateMenu(params) {
    return request({
        url: "/service/system/menu/saveOrUpdate",
        method: "post",
        data: params
    });
}