// 充值审核相关接口
import request from '@/utils/request'

var api = {}

api.add = function add(obj) {
  return request({
    url: '/service/member/member-charge/add',
    method: 'put',
    data: obj
  })
}

api.del = function del(id) {
  return request({
    url: `/service/member/member-charge/delete/?id=${id}`,
    method: 'delete'
  })
}

api.update = function update(obj) {
  return request({
    url: '/service/member/member-charge/update',
    method: 'post',
    data: obj
  })
}

api.list = function list(obj) {
  return request({
    url: `/service/member/member-charge/list`,
    method: 'get',
    params: obj
  })
}

api.audit = function audit(data) {
  return request({
    url: `/service/member/member-charge/check`,
    method: 'post',
    data
  })
}

export default api
