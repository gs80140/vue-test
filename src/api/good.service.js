import request from "@/utils/request";

// 商品列表
export function productList(params) {
    return request({
        url: "/service/goods/list",
        method: "get",
        params: params
    });
}

//添加商品
export function addProduct(params) {
    return request({
        url: "/service/goods/add",
        method: "PUT",
        data: params
    });
}
