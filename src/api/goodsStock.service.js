// 幻灯片相关接口
import request from '@/utils/request'

var api = {}

// 入库接口
api.inStock = function add(obj) {
  return request({
    url: '/service/stock/goods-stock/inStock',
    method: 'put',
    data: obj
  })
}

api.del = function del(params) {
  return request({
    url: `/service/stock/goods-stock/delete`,
    method: 'delete',
    params
  })
}

// 出库接口
api.outStock = function update(obj) {
  return request({
    url: '/service/stock/goods-stock/outStock',
    method: 'post',
    data: obj
  })
}

// 库存列表
api.list = function list(obj) {
  return request({
    url: `/service/stock/goods-stock/list/`,
    method: 'get',
    params: obj
  })
}

// 店铺仓库列表
api.storeList = function list(obj) {
  return request({
    url: `/service/stock/store-stock/list`,
    method: 'get',
    params: obj
  })
}

// 根据条形码获取详情
api.getDetailByBarCode = function list(obj) {
  return request({
    url: `/service/stock/goods-stock/getDetailByBarCode`,
    method: 'get',
    params: obj
  })
}

// 导出库存
api.exportStcok = function list(obj) {
  return request({
    url: `/service/stock/goods-stock/exportStcok`,
    method: 'get',
    params: obj
  })
}

export default api
