// 幻灯片相关接口
import request from '@/utils/request'

var api = {}

api.add = function add(obj) {
  return request({
    url: '/service/basic/cat-duty-bind/add',
    method: 'put',
    data: obj
  })
}

api.del = function del(id) {
  return request({
    url: `/service/basic/cat-duty-bind/delete/?id=${id}`,
    method: 'delete'
  })
}

api.update = function update(obj) {
  return request({
    url: '/service/basic/cat-duty-bind/update',
    method: 'post',
    data: obj
  })
}

api.list = function list(obj) {
  return request({
    url: `/service/basic/cat-duty-bind/list/?${obj}`,
    method: 'get',
    params: obj
  })
}

export default api
