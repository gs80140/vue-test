import request from "@/utils/request";

export function commitGerenate(params) {
    return request({
      url: "/service/tools/generate",
      method: "post",
      data: params
    });
  }

export function getGenerateInfor() {
  return request({
    url: `/service/tools/generate`,
    method: "get"
  });
}
export function getTableInfor(params) {
  return request({
    url: `/service/tools/tables`,
    method: "get",
    params: params
  });
}

export function listByParentId(params) {
  return request({
    url: `/service/system/menu/list-by-parentid`,
    method: 'get',
    params: { parentId: 0 }
  })
}
