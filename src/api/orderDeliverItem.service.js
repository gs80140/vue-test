// 幻灯片相关接口
import request from '@/utils/request'

var api = {}

api.add = function add(obj) {
  return request({
    url: '/service/stock/order-deliver-item/add',
    method: 'put',
    data: obj
  })
}

api.del = function del(id) {
  return request({
    url: `/service/stock/order-deliver-item/delete/?id=${id}`,
    method: 'delete'
  })
}

// 核验发货
api.checkDeliver = function update(obj) {
  return request({
    url: '/service/order/order-deliver/checkDeliver',
    method: 'post',
    data: obj
  })
}

// 获取订单分包发货明细列表
api.list = function list(obj) {
  return request({
    url: `/service/order/order-deliver-item/list`,
    method: 'get',
    params: obj
  })
}

// 获取订单分包发货列表
api.orderList = function list(obj) {
  return request({
    url: `/service/order/order-deliver/list`,
    method: 'get',
    params: obj
  })
}

// 根据包裹号查询商品信息
api.getGoodsByPackageNo = function list(obj) {
  return request({
    url: `/service/order/order-deliver/getGoodsByPackageNo`,
    method: 'get',
    params: obj
  })
}

// 店铺仓库列表
api.storeList = function list(obj) {
  return request({
    url: `/service/stock/store-stock/list`,
    method: 'get',
    params: obj
  })
}

// 根据条形码获取详情
api.getDetailByBarCode = function list(obj) {
  return request({
    url: `/service/stock/goods-stock/getDetailByBarCode`,
    method: 'get',
    params: obj
  })
}

// 打印A网快递单
api.getAPrintData = function update(obj) {
  return request({
    url: '/service/order/order-deliver/getAPrintData',
    method: 'post',
    data: obj
  })
}

// 打印B网快递单
api.getBPrintData = function update(obj) {
  return request({
    url: '/service/order/order-deliver/getBPrintData',
    method: 'post',
    data: obj
  })
}

export default api
