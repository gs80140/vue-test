// 幻灯片相关接口
import request from '@/utils/request'

var api = {}

api.add = function add(obj) {
  return request({
    url: '/service/basic/duty-paid-price/add',
    method: 'put',
    data: obj
  })
}

api.del = function del(id) {
  return request({
    url: `/service/basic/duty-paid-price/delete/?id=${id}`,
    method: 'delete'
  })
}

api.update = function update(obj) {
  return request({
    url: '/service/basic/duty-paid-price/update',
    method: 'post',
    data: obj
  })
}

api.list = function list(obj) {
  return request({
    url: `/service/basic/duty-paid-price/list/?${obj}`,
    method: 'get',
    params: obj
  })
}

export default api
