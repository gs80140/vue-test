// 幻灯片相关接口
import request from '@/utils/request'

var api = {}

api.add = function add(obj) {
  return request({
    url: '/service/basic/payment/add',
    method: 'put',
    data: obj
  })
}

api.del = function del(id) {
  return request({
    url: `/service/basic/payment/delete/?id=${id}`,
    method: 'delete'
  })
}

api.update = function update(obj) {
  return request({
    url: '/service/basic/payment/update',
    method: 'post',
    data: obj
  })
}

api.list = function list(obj) {
  return request({
    url: `/service/basic/payment/list`,
    method: 'get',
    params: obj
  })
}

export default api
