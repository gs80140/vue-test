const getters = {
    sidebar: state => state.app.sidebar,
    device: state => state.app.device,
    token: state => state.user.token,
    id: state => state.user.id,
    avatar: state => state.user.avatar,
    name: state => state.user.name,
    username: state => state.user.username,
    roleId: state => state.user.roleId,
    permission_routers: state => state.user.routers,
    addRouters: state => state.user.addRouters,
    visitedViews: state => state.tagsView.visitedViews,
    cachedViews: state => state.tagsView.cachedViews,
}
export default getters