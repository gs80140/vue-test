export default {
  zh: {
    memberCharge: {
      search: {
        keyWords: '关键词'
      },
      tableHead: {
        id: 'ID',
        userId: '用户ID',
        account: '账号',
        money: '充值金额',
        status: '状态',
        orderNo: '充值单号',
        picture: '截图图片',
        remark: '审核留言',
        addTime: '添加时间',
        updateTime: '更新时间'
      }
    }
  },
  en: {
    memberCharge: {
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
        // TODO 需要翻译成英文
        id: 'ID',
        userId: 'userId',
        account: 'account',
        money: 'money',
        status: 'status',
        orderNo: 'orderNo',
        picture: 'picture',
        remark: 'remark',
        addTime: 'addTime',
        updateTime: 'updateTime'
      }
    }
  },
  ko: {
    memberCharge: {
      search: {
        keyWords: '키워드'
      },
      tableHead: {
        // TODO 需要翻译成韩文
        id: 'ID',
        userId: '사용자 ID',
        account: '계좌 번호',
        money: '충전 금액',
        status: '상태',
        orderNo: '충전 번호',
        picture: '캡처 그림',
        remark: '메모를 심의하다.',
        addTime: '시간 추가',
        updateTime: '업데이트 시간'
      }
    }
  }
}
