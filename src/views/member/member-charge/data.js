// 定制表头显示的数据，prop需要与表格数据对应
const tableHeaderData = {
  hasSelection: true, // 是否可以多选
  data: [
    { label: 'id', prop: 'id' },
    { label: '用户ID', prop: 'userId' },
    { label: '账号', prop: 'account' },
    { label: '充值金额', prop: 'money' },
    { label: '状态', prop: 'status' },
    { label: '充值单号', prop: 'orderNo' },
    { label: '截图图片', prop: 'picture' },
    { label: '审核留言', prop: 'remark' },
    { label: '添加时间', prop: 'addTime' },
    { label: '更新时间', prop: 'updateTime' }
  ]
}

const searchData = [
  { desc: '关键词', type: 'input', key: 'keyWords' }
  // { desc: '时间选择', type: 'dateTime', key: 'dateTime' },
  // { desc: '状态', type: 'select', key: 'isShow', options: [{ value: '1', label: '是' }, { value: '0', label: '否' }] }
]

const name = 'memberCharge'

export {
  tableHeaderData, // 表头配置
  searchData, // 查询配置
  name // 文件名，为国际化用
}
