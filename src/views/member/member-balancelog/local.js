export default {
  zh: {
    memberBalanceLog: {
      search: {
        keyWords: '关键词'
      },
      tableHead: {
           id:'',
           userId:'用户ID',
           userName:'账号',
           leftMoney:'剩余金额',
           changeMoney:'金额变化',
           remark:'事件记录',
           addTime:'添加时间',
      }
    }
  },
  en: {
    memberBalanceLog: {
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
       //TODO 需要翻译成英文
         id:'',
         userId:'用户ID',
         userName:'账号',
         leftMoney:'剩余金额',
         changeMoney:'金额变化',
         remark:'事件记录',
         addTime:'添加时间',
      }
    }
  },
  ko: {
    memberBalanceLog: {
      search: {
        keyWords: '키워드'
      },
      tableHead: {
       //TODO 需要翻译成韩文
         id:'',
         userId:'用户ID',
         userName:'账号',
         leftMoney:'剩余金额',
         changeMoney:'金额变化',
         remark:'事件记录',
         addTime:'添加时间',
      }
    }
  }
}
