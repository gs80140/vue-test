// 定制表头显示的数据，prop需要与表格数据对应
const tableHeaderData = {
    hasSelection: false, //是否可以多选
    data: [
        { label: '', prop: 'id' },
        { label: '用户ID', prop: 'userId' },
        { label: '账号', prop: 'userName' },
        { label: '剩余金额', prop: 'leftMoney' },
        { label: '金额变化', prop: 'changeMoney' },
        { label: '事件记录', prop: 'remark' },
        { label: '添加时间', prop: 'addTime' },
    ]
}


const searchData = [
  { desc: '关键词', type: 'input', key: 'keyWords' },
  //{ desc: '时间选择', type: 'dateTime', key: 'dateTime' },
  //{ desc: '状态', type: 'select', key: 'isShow', options: [{ value: '1', label: '是' }, { value: '0', label: '否' }] }
]

const tableData = []

const name = 'memberBalanceLog'

export {
  tableHeaderData, // 表头配置
  searchData, // 查询配置
  tableData, // 表格数据
  name // 文件名，为国际化用
}