// 定制表头显示的数据，prop需要与表格数据对应
const tableHeaderData = {
    hasSelection: false, //是否可以多选
    data: [
        { label: '用户ID', prop: 'id' },
        { label: '用户名,登陆名', prop: 'userName' },
        { label: '账号密码', prop: 'password' },
        { label: '手机号', prop: 'mobile' },
        { label: '支付密码', prop: 'payPassword' },
        { label: '真实姓名', prop: 'trueName' },
        { label: '性别,0:保密,1:男,2:女', prop: 'sex' },
        { label: '生日', prop: 'birthday' },
        { label: '邮箱', prop: 'email' },
        { label: '是否绑定邮箱', prop: 'emailBind' },
        { label: '头像', prop: 'avatar' },
        { label: '支付宝账号', prop: 'alipay' },
        { label: '微信号', prop: 'wechat' },
        { label: '身份证号', prop: 'idCard' },
        { label: '邀请人ID', prop: 'inviteId' },
        { label: '注册IP地址', prop: 'registryIp' },
        { label: 'salt,密码加密', prop: 'salt' },
        { label: '注册时间', prop: 'addTime' },
    ]
}


const searchData = [
  { desc: '关键词', type: 'input', key: 'keyWords' },
  //{ desc: '时间选择', type: 'dateTime', key: 'dateTime' },
  //{ desc: '状态', type: 'select', key: 'isShow', options: [{ value: '1', label: '是' }, { value: '0', label: '否' }] }
]

const tableData = []

const name = 'member'

export {
  tableHeaderData, // 表头配置
  searchData, // 查询配置
  tableData, // 表格数据
  name // 文件名，为国际化用
}