export default {
  zh: {
    member: {
      search: {
        keyWords: '关键词'
      },
      tableHead: {
           id:'用户ID',
           userName:'用户名,登陆名',
           password:'账号密码',
           mobile:'手机号',
           payPassword:'支付密码',
           trueName:'真实姓名',
           sex:'性别,0:保密,1:男,2:女',
           birthday:'生日',
           email:'邮箱',
           emailBind:'是否绑定邮箱',
           avatar:'头像',
           alipay:'支付宝账号',
           wechat:'微信号',
           idCard:'身份证号',
           inviteId:'邀请人ID',
           registryIp:'注册IP地址',
           salt:'salt,密码加密',
           addTime:'注册时间',
      }
    }
  },
  en: {
    member: {
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
       //TODO 需要翻译成英文
         id:'用户ID',
         userName:'用户名,登陆名',
         password:'账号密码',
         mobile:'手机号',
         payPassword:'支付密码',
         trueName:'真实姓名',
         sex:'性别,0:保密,1:男,2:女',
         birthday:'生日',
         email:'邮箱',
         emailBind:'是否绑定邮箱',
         avatar:'头像',
         alipay:'支付宝账号',
         wechat:'微信号',
         idCard:'身份证号',
         inviteId:'邀请人ID',
         registryIp:'注册IP地址',
         salt:'salt,密码加密',
         addTime:'注册时间',
      }
    }
  },
  ko: {
    member: {
      search: {
        keyWords: '키워드'
      },
      tableHead: {
       //TODO 需要翻译成韩文
         id:'用户ID',
         userName:'用户名,登陆名',
         password:'账号密码',
         mobile:'手机号',
         payPassword:'支付密码',
         trueName:'真实姓名',
         sex:'性别,0:保密,1:男,2:女',
         birthday:'生日',
         email:'邮箱',
         emailBind:'是否绑定邮箱',
         avatar:'头像',
         alipay:'支付宝账号',
         wechat:'微信号',
         idCard:'身份证号',
         inviteId:'邀请人ID',
         registryIp:'注册IP地址',
         salt:'salt,密码加密',
         addTime:'注册时间',
      }
    }
  }
}
