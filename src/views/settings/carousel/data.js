// 定制表头显示的数据，prop需要与表格数据对应
export const tableHeaderData = {
    hasSelection: false, //是否可以多选
    data: [
        { label: 'ID', prop: 'id' },
        { label: '名称', prop: 'name' },
        { label: '缩略图', prop: 'thumbnail' },
        { label: '价格', prop: 'price' },
        { label: '数量', prop: 'number' },
    ]
}

export const tableData = [{
        id: "001",
        name: "名称1",
        thumbnail: "http://wq.9zgw.com/attachment/images/1/2018/12/yR26ZL8V682XrOr4M8VGC81ZooGx1W2O.jpg",
        price: "109.00",
        number: "99",
    },
    {
        id: "002",
        name: "名称2",
        thumbnail: "http://wq.9zgw.com/attachment/images/1/2018/12/yR26ZL8V682XrOr4M8VGC81ZooGx1W2O.jpg",
        price: "109.00",
        number: "3214",
    },
    {
        id: "003",
        name: "名称3",
        thumbnail: "http://wq.9zgw.com/attachment/images/1/2018/12/yR26ZL8V682XrOr4M8VGC81ZooGx1W2O.jpg",
        price: "109.00",
        number: "213"
    },
    {
        id: "004",
        name: "名称4",
        thumbnail: "http://wq.9zgw.com/attachment/images/1/2018/12/yR26ZL8V682XrOr4M8VGC81ZooGx1W2O.jpg",
        price: "109.00",
        number: "43"
    },
    {
        id: "005",
        name: "名称5",
        thumbnail: "http://wq.9zgw.com/attachment/images/1/2018/12/yR26ZL8V682XrOr4M8VGC81ZooGx1W2O.jpg",
        price: "109.00",
        number: "78"
    },
    {
        id: "006",
        name: "名称16",
        thumbnail: "http://wq.9zgw.com/attachment/images/1/2018/12/yR26ZL8V682XrOr4M8VGC81ZooGx1W2O.jpg",
        price: "109.00",
        number: "89"
    },
    {
        id: "007",
        name: "名称7",
        thumbnail: "http://wq.9zgw.com/attachment/images/1/2018/12/yR26ZL8V682XrOr4M8VGC81ZooGx1W2O.jpg",
        price: "109.00",
        number: "90"
    }
]