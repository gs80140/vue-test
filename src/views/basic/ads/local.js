
export default {
  zh: {
    ads: {
      search: {
        keyWords: '关键词',
        isShow: '状态',
        dateTime: '日期时间'
      },
      tableHead: {
        sort: '排序',
        isShow: '状态',
        id: 'ID',
        link: '链接网址',
        location: '广告位置',
        locationDesc: '广告位置描述',
        openType: '打开方式',
        picture: '图片网址'
      }
    }
  },
  en: {
    ads: {
      search: {
        keyWords: 'keyWords',
        isShow: 'isShow',
        dateTime: 'dateTime'
      },
      tableHead: {
        sort: 'sort',
        isShow: 'isShow',
        id: 'id',
        link: 'link',
        location: 'location',
        locationDesc: 'locationDesc',
        openType: 'openType',
        picture: 'picture'
      }
    }
  },
  ko: {
    ads: {
      search: {
        keyWords: '키워드',
        isShow: '상태'
      },
      tableHead: {
        sort: '정렬',
        isShow: '상태',
        id: '아이디',
        link: '네트워크 주소',
        location: '광고 위치',
        locationDesc: '광고 위치 설명',
        openType: '열기 방식',
        picture: '그림 사이트 주소'
      }
    }
  }
}
