// 定制表头显示的数据，prop需要与表格数据对应
const tableHeaderData = {
  hasSelection: true, // 是否可以多选
  data: [
    { label: 'ID', prop: 'id' },
    { label: '是否显示', prop: 'isShow' },
    { label: '链接网址', prop: 'link', type: 'link' },
    { label: '广告位置', prop: 'location' },
    { label: '广告位置描述', prop: 'locationDesc' },
    { label: '打开方式', prop: 'openType' },
    { label: '图片网址', prop: 'picture', type: 'img' },
    { label: '排序', prop: 'sort' }
  ]
}

const searchData = [
  { desc: '关键词', type: 'input', key: 'keyWords' },
  { desc: '日期时间', type: 'dateTime', key: 'dateTime' },
  { desc: '状态', type: 'select', key: 'isShow', options: [{ value: '1', label: '是' }, { value: '0', label: '否' }] }
]

const tableData = []

const name = 'ads'

export {
  tableHeaderData, // 表头配置
  searchData, // 查询配置
  tableData, // 表格数据
  name // 文件名，为国际化用
}
