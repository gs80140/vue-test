export default {
  zh: {
    goodsCats: {
      search: {
        keyWords: '关键词'
      },
      tableHead: {
           id:'商品所属类目ID',
           parentId:'父类目ID',
           name:'类目名称',
           isParent:'该类目是否为父类目(即：该类目是否还有子类目)',
           level:'等级,第一层,第二层..',
      }
    }
  },
  en: {
    goodsCats: {
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
       //TODO 需要翻译成英文
         id:'商品所属类目ID',
         parentId:'父类目ID',
         name:'类目名称',
         isParent:'该类目是否为父类目(即：该类目是否还有子类目)',
         level:'等级,第一层,第二层..',
      }
    }
  },
  ko: {
    goodsCats: {
      search: {
        keyWords: '키워드'
      },
      tableHead: {
       //TODO 需要翻译成韩文
         id:'商品所属类目ID',
         parentId:'父类目ID',
         name:'类目名称',
         isParent:'该类目是否为父类目(即：该类目是否还有子类目)',
         level:'等级,第一层,第二层..',
      }
    }
  }
}
