export default {
  zh: {
    expressCompany: {
      search: {
        keyWords: '关键词'
      },
      tableHead: {
           id:'ID',
           expressCompany:'快递公司名',
           expressCode:'快递公司编码',
      }
    }
  },
  en: {
    expressCompany: {
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
       //TODO 需要翻译成英文
         id:'ID',
         expressCompany:'快递公司名',
         expressCode:'快递公司编码',
      }
    }
  },
  ko: {
    expressCompany: {
      search: {
        keyWords: '키워드'
      },
      tableHead: {
       //TODO 需要翻译成韩文
         id:'ID',
         expressCompany:'快递公司名',
         expressCode:'快递公司编码',
      }
    }
  }
}
