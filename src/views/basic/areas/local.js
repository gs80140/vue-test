export default {
  zh: {
    areas: {
      search: {
        keyWords: '关键词'
      },
      tableHead: {
           id:'标准行政区域代码',
           type:'区域类型.area区域 1:country/国家;2:province/省/自治区/直辖市;3:city/地区(省下面的地级市);4:district/县/市(县级市)/区;abroad:海外. 比如北京市的area_type = 2,朝阳区是北京市的一个区,所以朝阳区的area_type = 4.',
           name:'地域名称.如北京市,杭州市,西湖区,每一个area_id 都代表了一个具体的地区.',
           parentId:'父节点区域标识.如北京市的area_id是110100,朝阳区是北京市的一个区,所以朝阳区的parent_id就是北京市的area_id.',
           zip:'具体一个地区的邮编',
      }
    }
  },
  en: {
    areas: {
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
       //TODO 需要翻译成英文
         id:'标准行政区域代码',
         type:'区域类型.area区域 1:country/国家;2:province/省/自治区/直辖市;3:city/地区(省下面的地级市);4:district/县/市(县级市)/区;abroad:海外. 比如北京市的area_type = 2,朝阳区是北京市的一个区,所以朝阳区的area_type = 4.',
         name:'地域名称.如北京市,杭州市,西湖区,每一个area_id 都代表了一个具体的地区.',
         parentId:'父节点区域标识.如北京市的area_id是110100,朝阳区是北京市的一个区,所以朝阳区的parent_id就是北京市的area_id.',
         zip:'具体一个地区的邮编',
      }
    }
  },
  ko: {
    areas: {
      search: {
        keyWords: '키워드'
      },
      tableHead: {
       //TODO 需要翻译成韩文
         id:'标准行政区域代码',
         type:'区域类型.area区域 1:country/国家;2:province/省/自治区/直辖市;3:city/地区(省下面的地级市);4:district/县/市(县级市)/区;abroad:海外. 比如北京市的area_type = 2,朝阳区是北京市的一个区,所以朝阳区的area_type = 4.',
         name:'地域名称.如北京市,杭州市,西湖区,每一个area_id 都代表了一个具体的地区.',
         parentId:'父节点区域标识.如北京市的area_id是110100,朝阳区是北京市的一个区,所以朝阳区的parent_id就是北京市的area_id.',
         zip:'具体一个地区的邮编',
      }
    }
  }
}
