// 定制表头显示的数据，prop需要与表格数据对应
const tableHeaderData = {
    hasSelection: false, //是否可以多选
    data: [
        { label: '标准行政区域代码', prop: 'id' },
        { label: '区域类型.area区域 1:country/国家;2:province/省/自治区/直辖市;3:city/地区(省下面的地级市);4:district/县/市(县级市)/区;abroad:海外. 比如北京市的area_type = 2,朝阳区是北京市的一个区,所以朝阳区的area_type = 4.', prop: 'type' },
        { label: '地域名称.如北京市,杭州市,西湖区,每一个area_id 都代表了一个具体的地区.', prop: 'name' },
        { label: '父节点区域标识.如北京市的area_id是110100,朝阳区是北京市的一个区,所以朝阳区的parent_id就是北京市的area_id.', prop: 'parentId' },
        { label: '具体一个地区的邮编', prop: 'zip' },
    ]
}


const searchData = [
  { desc: '关键词', type: 'input', key: 'keyWords' },
  //{ desc: '时间选择', type: 'dateTime', key: 'dateTime' },
  //{ desc: '状态', type: 'select', key: 'isShow', options: [{ value: '1', label: '是' }, { value: '0', label: '否' }] }
]

const tableData = []

const name = 'areas'

export {
  tableHeaderData, // 表头配置
  searchData, // 查询配置
  tableData, // 表格数据
  name // 文件名，为国际化用
}