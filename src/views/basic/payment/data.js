// 定制表头显示的数据，prop需要与表格数据对应
const tableHeaderData = {
    hasSelection: false, //是否可以多选
    data: [
        { label: '', prop: 'id' },
        { label: 'app类型:h5,pc', prop: 'appType' },
        { label: '支付名称', prop: 'payName' },
        { label: '支付图标', prop: 'picture' },
        { label: '支付类型,alipay:支付宝,wxpay:微信,yinlian:银联支付', prop: 'payType' },
        { label: '支付方式数据json格式', prop: 'payData' },
        { label: '排序', prop: 'sortNum' },
    ]
}


const searchData = [
  { desc: '关键词', type: 'input', key: 'keyWords' },
  //{ desc: '时间选择', type: 'dateTime', key: 'dateTime' },
  //{ desc: '状态', type: 'select', key: 'isShow', options: [{ value: '1', label: '是' }, { value: '0', label: '否' }] }
]

const tableData = []

const name = 'payment'

export {
  tableHeaderData, // 表头配置
  searchData, // 查询配置
  tableData, // 表格数据
  name // 文件名，为国际化用
}