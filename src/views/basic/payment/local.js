export default {
  zh: {
    payment: {
      search: {
        keyWords: '关键词'
      },
      tableHead: {
           id:'',
           appType:'app类型:h5,pc',
           payName:'支付名称',
           picture:'支付图标',
           payType:'支付类型,alipay:支付宝,wxpay:微信,yinlian:银联支付',
           payData:'支付方式数据json格式',
           sortNum:'排序',
      }
    }
  },
  en: {
    payment: {
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
       //TODO 需要翻译成英文
         id:'',
         appType:'app类型:h5,pc',
         payName:'支付名称',
         picture:'支付图标',
         payType:'支付类型,alipay:支付宝,wxpay:微信,yinlian:银联支付',
         payData:'支付方式数据json格式',
         sortNum:'排序',
      }
    }
  },
  ko: {
    payment: {
      search: {
        keyWords: '키워드'
      },
      tableHead: {
       //TODO 需要翻译成韩文
         id:'',
         appType:'app类型:h5,pc',
         payName:'支付名称',
         picture:'支付图标',
         payType:'支付类型,alipay:支付宝,wxpay:微信,yinlian:银联支付',
         payData:'支付方式数据json格式',
         sortNum:'排序',
      }
    }
  }
}
