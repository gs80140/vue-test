// 定制表头显示的数据，prop需要与表格数据对应
const tableHeaderData = {
    hasSelection: false, //是否可以多选
    data: [
        { label: 'ID', prop: 'id' },
        { label: '税号', prop: 'taxNo' },
        { label: '品名及规格', prop: 'taxName' },
        { label: '单位码', prop: 'unitCode' },
        { label: '单位', prop: 'taxUnit' },
        { label: '完税价格', prop: 'taxPrice' },
        { label: '税率', prop: 'taxRate' },
    ]
}


const searchData = [
  { desc: '关键词', type: 'input', key: 'keyWords' },
  //{ desc: '时间选择', type: 'dateTime', key: 'dateTime' },
  //{ desc: '状态', type: 'select', key: 'isShow', options: [{ value: '1', label: '是' }, { value: '0', label: '否' }] }
]

const tableData = []

const name = 'dutyPaidPrice'

export {
  tableHeaderData, // 表头配置
  searchData, // 查询配置
  tableData, // 表格数据
  name // 文件名，为国际化用
}