export default {
  zh: {
    dutyPaidPrice: {
      search: {
        keyWords: '关键词'
      },
      tableHead: {
           id:'ID',
           taxNo:'税号',
           taxName:'品名及规格',
           unitCode:'单位码',
           taxUnit:'单位',
           taxPrice:'完税价格',
           taxRate:'税率',
      }
    }
  },
  en: {
    dutyPaidPrice: {
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
       //TODO 需要翻译成英文
         id:'ID',
         taxNo:'税号',
         taxName:'品名及规格',
         unitCode:'单位码',
         taxUnit:'单位',
         taxPrice:'完税价格',
         taxRate:'税率',
      }
    }
  },
  ko: {
    dutyPaidPrice: {
      search: {
        keyWords: '키워드'
      },
      tableHead: {
       //TODO 需要翻译成韩文
         id:'ID',
         taxNo:'税号',
         taxName:'品名及规格',
         unitCode:'单位码',
         taxUnit:'单位',
         taxPrice:'完税价格',
         taxRate:'税率',
      }
    }
  }
}
