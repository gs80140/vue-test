export default {
  zh: {
    catDutyBind: {
      search: {
        keyWords: '关键词'
      },
      tableHead: {
           id:'ID',
           catId:'分类号',
           catNameEn:'分类英文名',
           catNameCn:'分类中文名',
           taxNo:'税号',
      }
    }
  },
  en: {
    catDutyBind: {
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
       //TODO 需要翻译成英文
         id:'ID',
         catId:'分类号',
         catNameEn:'分类英文名',
         catNameCn:'分类中文名',
         taxNo:'税号',
      }
    }
  },
  ko: {
    catDutyBind: {
      search: {
        keyWords: '키워드'
      },
      tableHead: {
       //TODO 需要翻译成韩文
         id:'ID',
         catId:'分类号',
         catNameEn:'分类英文名',
         catNameCn:'分类中文名',
         taxNo:'税号',
      }
    }
  }
}
