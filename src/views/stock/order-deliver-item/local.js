export default {
  zh: {
    orderDeliverItem: {
      search: {
        packageNo: '包裹号',
        orderNo: '订单号',
        orderItemId: '订单明细ID',
        expressNo: '快递单号',
        deliverStatus: '出库状态'
      },
      tableHead: {
        id: 'ID',
        packageNo: '包裹编号',
        orderNo: '订单号',
        orderId: '订单ID',
        orderItemId: '订单明细ID',
        goodsName: '商品名',
        goodsNum: '商品数量',
        expressCompany: '快递公司',
        expressNo: '快递单号',
        skuId: 'skuId',
        skuName: 'sku规格名称',
        picture: '图片地址',
        addTime: '添加时间',
        updateTime: '更新时间',
        deliverStatus: '真实出库状态,0:未出库,1:已出库',
        remark: '备注'
      }
    }
  },
  en: {
    orderDeliverItem: {
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
        // TODO 需要翻译成英文
        id: 'ID',
        packageNo: '包裹编号',
        orderNo: '订单号',
        orderId: '订单ID',
        orderItemId: '订单明细ID',
        goodsName: '商品名',
        goodsNum: '商品数量',
        skuId: 'skuId',
        skuName: 'sku规格名称',
        picture: '图片地址',
        addTime: '添加时间',
        updateTime: '更新时间',
        deliverStatus: '真实出库状态,0:未出库,1:已出库',
        remark: '备注'
      }
    }
  },
  ko: {
    orderDeliverItem: {
      search: {
        keyWords: '키워드'
      },
      tableHead: {
        // TODO 需要翻译成韩文
        id: 'ID',
        packageNo: '包裹编号',
        orderNo: '订单号',
        orderId: '订单ID',
        orderItemId: '订单明细ID',
        goodsName: '商品名',
        goodsNum: '商品数量',
        skuId: 'skuId',
        skuName: 'sku规格名称',
        picture: '图片地址',
        addTime: '添加时间',
        updateTime: '更新时间',
        deliverStatus: '真实出库状态,0:未出库,1:已出库',
        remark: '备注'
      }
    }
  }
}
