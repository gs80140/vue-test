// 定制表头显示的数据，prop需要与表格数据对应
const tableHeaderData = {
  hasSelection: false, // 是否可以多选
  data: [
    { label: 'ID', prop: 'id' },
    { label: '包裹编号', prop: 'packageNo' },
    { label: '订单号', prop: 'orderNo' },
    { label: '订单ID', prop: 'orderId' },
    { label: '订单明细ID', prop: 'orderItemId' },
    { label: '商品名', prop: 'goodsName', width: '350' },
    { label: '快递公司', prop: 'expressCompany' },
    { label: '商品数量', prop: 'goodsNum' },
    { label: '快递单号', prop: 'expressNo' },
    { label: 'skuId', prop: 'skuId' },
    { label: 'sku规格名称', prop: 'skuName' },
    { label: '图片地址', prop: 'picture', type: 'img', width: '150' },
    { label: '添加时间', prop: 'addTime' },
    { label: '更新时间', prop: 'updateTime' },
    { label: '真实出库状态,0:未出库,1:已出库', prop: 'deliverStatus' },
    { label: '备注', prop: 'remark' }
  ]
}

const searchData = [
  { desc: '包裹号', type: 'input', key: 'packageNo' },
  { desc: '订单号', type: 'input', key: 'orderNo' },
  { desc: '订单Id', type: 'input', key: 'orderItemId' },
  { desc: '快递单号', type: 'input', key: 'expressNo' },
  {
    desc: '出库状态',
    type: 'select',
    key: 'deliverStatus',
    options: [{ value: null, label: '所有' }, { value: 0, label: '未出库' }, { value: 1, label: '已出库' }]
  }
]

const name = 'orderDeliverItem'

export {
  tableHeaderData, // 表头配置
  searchData, // 查询配置
  name // 文件名，为国际化用
}
