// 定制表头显示的数据，prop需要与表格数据对应
const tableHeaderData = {
  hasSelection: false, // 是否可以多选
  data: [
    { label: 'ID', prop: 'id' },
    { label: 'SKU码', prop: 'skuId' },
    { label: '商品ID', prop: 'goodsId' },
    { label: '商品SKU,黑色+L', prop: 'goodsSku' },
    { label: '仓库ID', prop: 'stockId' },
    { label: '仓库名称', prop: 'stockName' },
    { label: '条形码', prop: 'barCode' },
    { label: '库存', prop: 'stock' },
    { label: '预留数量', prop: 'reserveStock' },
    { label: '添加时间', prop: 'addTime' },
    { label: '更新时间', prop: 'updateTime' }
  ]
}

const searchData = [
  { desc: '商品编码', type: 'input', key: 'goodsId' },
  { desc: 'SKU码', type: 'input', key: 'skuId' },
  { desc: '变动日期', type: 'dateTime', key: 'updateTimeRange' }
]

const name = 'goodsStock'

export {
  tableHeaderData, // 表头配置
  searchData, // 查询配置
  name // 文件名，为国际化用
}
