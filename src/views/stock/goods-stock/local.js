export default {
  zh: {
    goodsStock: {
      search: {
        goodsId: '商品编码',
        skuId: 'SKU码',
        updateTimeRange: '变动日期'
      },
      tableHead: {
        id: 'ID',
        skuId: 'SKU码',
        goodsId: '商品ID',
        goodsSku: '商品SKU,黑色+L',
        stockId: '仓库ID',
        stockName: '仓库名称',
        barCode: '条形码',
        stock: '库存',
        reserveStock: '预留数量',
        addTime: '添加时间',
        updateTime: '更新时间'
      }
    }
  },
  en: {
    goodsStock: {
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
        // TODO 需要翻译成英文
        id: 'ID',
        skuId: 'SKU码',
        goodsId: '商品ID',
        goodsSku: '商品SKU,黑色+L',
        stockId: '仓库ID',
        stockName: '仓库名称',
        barCode: '条形码',
        stock: '库存',
        reserveStock: '预留数量',
        addTime: '添加时间',
        updateTime: '更新时间'
      }
    }
  },
  ko: {
    goodsStock: {
      search: {
        keyWords: '키워드'
      },
      tableHead: {
        // TODO 需要翻译成韩文
        id: 'ID',
        skuId: 'SKU码',
        goodsId: '商品ID',
        goodsSku: '商品SKU,黑色+L',
        stockId: '仓库ID',
        stockName: '仓库名称',
        barCode: '条形码',
        stock: '库存',
        reserveStock: '预留数量',
        addTime: '添加时间',
        updateTime: '更新时间'
      }
    }
  }
}
