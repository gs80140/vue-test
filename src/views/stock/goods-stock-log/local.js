export default {
  zh: {
    goodsStockLog: {
      search: {
        goodsId: '商品编码',
        skuId: 'SKU码'
      },
      tableHead: {
        id: 'id',
        goodsStockId: '库存ID',
        skuId: 'skuId',
        goodsId: '商品编码',
        oldStock: '旧的库存',
        stock: '库存',
        type: '-1:出库,1:入库,0:库存维护',
        addBy: '添加人',
        addTime: '添加时间',
        remark: '备注'
      }
    }
  },
  en: {
    goodsStockLog: {
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
        // TODO 需要翻译成英文
        id: 'id',
        goodsStockId: '库存ID',
        oldStock: '旧的库存',
        stock: '库存',
        type: '-1:出库,1:入库,0:库存维护',
        addBy: '添加人',
        addTime: '添加时间',
        remark: '备注'
      }
    }
  },
  ko: {
    goodsStockLog: {
      search: {
        keyWords: '키워드'
      },
      tableHead: {
        // TODO 需要翻译成韩文
        id: 'id',
        goodsStockId: '库存ID',
        oldStock: '旧的库存',
        stock: '库存',
        type: '-1:出库,1:入库,0:库存维护',
        addBy: '添加人',
        addTime: '添加时间',
        remark: '备注'
      }
    }
  }
}
