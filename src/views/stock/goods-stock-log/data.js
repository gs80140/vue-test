// 定制表头显示的数据，prop需要与表格数据对应
const tableHeaderData = {
  hasSelection: false, // 是否可以多选
  data: [
    { label: 'id', prop: 'id' },
    { label: '库存ID', prop: 'goodsStockId' },
    { label: 'skuId', prop: 'skuId' },
    { label: '商品编码', prop: 'goodsId' },
    { label: '旧的库存', prop: 'oldStock' },
    { label: '库存', prop: 'stock' },
    { label: '-1:出库,1:入库,0:库存维护', prop: 'type' },
    { label: '添加人', prop: 'addBy' },
    { label: '添加时间', prop: 'addTime' },
    { label: '备注', prop: 'remark' }
  ]
}

const searchData = [
  { desc: 'SKU码', type: 'input', key: 'skuId' },
  { desc: '商品编码', type: 'input', key: 'goodsId' }
]

const name = 'goodsStockLog'

export {
  tableHeaderData, // 表头配置
  searchData, // 查询配置
  name // 文件名，为国际化用
}
