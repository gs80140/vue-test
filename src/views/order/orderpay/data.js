// 定制表头显示的数据，prop需要与表格数据对应
const tableHeaderData = {
    hasSelection: false, //是否可以多选
    data: [
        { label: '订单id', prop: 'orderId' },
        { label: '问题订单编号', prop: 'orderNo' },
        { label: '支付码', prop: 'payNo' },
        { label: '支付平台', prop: 'platform' },
        { label: '支付的钱数', prop: 'money' },
        { label: '状态,0:待支付,1:已支付', prop: 'status' },
        { label: '支付时间', prop: 'addTime' },
        { label: '更新时间', prop: 'udpateTime' },
    ]
}


const searchData = [
  { desc: '关键词', type: 'input', key: 'keyWords' },
  //{ desc: '时间选择', type: 'dateTime', key: 'dateTime' },
  //{ desc: '状态', type: 'select', key: 'isShow', options: [{ value: '1', label: '是' }, { value: '0', label: '否' }] }
]

const tableData = []

const name = 'orderPay'

export {
  tableHeaderData, // 表头配置
  searchData, // 查询配置
  tableData, // 表格数据
  name // 文件名，为国际化用
}