export default {
  zh: {
    orderPay: {
      search: {
        keyWords: '关键词'
      },
      tableHead: {
           orderId:'订单id',
           orderNo:'问题订单编号',
           payNo:'支付码',
           platform:'支付平台',
           money:'支付的钱数',
           status:'状态,0:待支付,1:已支付',
           addTime:'支付时间',
           udpateTime:'更新时间',
      }
    }
  },
  en: {
    orderPay: {
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
       //TODO 需要翻译成英文
         orderId:'订单id',
         orderNo:'问题订单编号',
         payNo:'支付码',
         platform:'支付平台',
         money:'支付的钱数',
         status:'状态,0:待支付,1:已支付',
         addTime:'支付时间',
         udpateTime:'更新时间',
      }
    }
  },
  ko: {
    orderPay: {
      search: {
        keyWords: '키워드'
      },
      tableHead: {
       //TODO 需要翻译成韩文
         orderId:'订单id',
         orderNo:'问题订单编号',
         payNo:'支付码',
         platform:'支付平台',
         money:'支付的钱数',
         status:'状态,0:待支付,1:已支付',
         addTime:'支付时间',
         udpateTime:'更新时间',
      }
    }
  }
}
