export default {
  zh: {
    shopCats: {
      search: {
        keyWords: '关键词'
      },
      tableHead: {
           id:'商品所属类目ID',
           storeId:'店铺ID',
           storeName:'店铺名',
           parentId:'父类目ID',
           name:'类目名称',
           isParent:'该类目是否为父类目(即：该类目是否还有子类目)',
           level:'等级,第一层,第二层..',
      }
    }
  },
  en: {
    shopCats: {
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
       //TODO 需要翻译成英文
         id:'商品所属类目ID',
         storeId:'店铺ID',
         storeName:'店铺名',
         parentId:'父类目ID',
         name:'类目名称',
         isParent:'该类目是否为父类目(即：该类目是否还有子类目)',
         level:'等级,第一层,第二层..',
      }
    }
  },
  ko: {
    shopCats: {
      search: {
        keyWords: '키워드'
      },
      tableHead: {
       //TODO 需要翻译成韩文
         id:'商品所属类目ID',
         storeId:'店铺ID',
         storeName:'店铺名',
         parentId:'父类目ID',
         name:'类目名称',
         isParent:'该类目是否为父类目(即：该类目是否还有子类目)',
         level:'等级,第一层,第二层..',
      }
    }
  }
}
