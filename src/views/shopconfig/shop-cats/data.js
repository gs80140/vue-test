// 定制表头显示的数据，prop需要与表格数据对应
const tableHeaderData = {
    hasSelection: false, //是否可以多选
    data: [
        { label: '商品所属类目ID', prop: 'id' },
        { label: '店铺ID', prop: 'storeId' },
        { label: '店铺名', prop: 'storeName' },
        { label: '父类目ID', prop: 'parentId' },
        { label: '类目名称', prop: 'name' },
        { label: '该类目是否为父类目(即：该类目是否还有子类目)', prop: 'isParent' },
        { label: '等级,第一层,第二层..', prop: 'level' },
    ]
}


const searchData = [
  { desc: '关键词', type: 'input', key: 'keyWords' },
  //{ desc: '时间选择', type: 'dateTime', key: 'dateTime' },
  //{ desc: '状态', type: 'select', key: 'isShow', options: [{ value: '1', label: '是' }, { value: '0', label: '否' }] }
]

const tableData = []

const name = 'shopCats'

export {
  tableHeaderData, // 表头配置
  searchData, // 查询配置
  tableData, // 表格数据
  name // 文件名，为国际化用
}