export default {
  zh: {
    freightTemplate: {
      sort: '排序',
      freightTempName: '运费模板名称',
      isDefault: '是否默认',
      yes: '是',
      no: '否',
      availability: '是否可用',
      billingMode: '计费模式',
      billing: '按件计费',
      chargingByWeight: '按重量计费',
      defaultFreight: '默认计费',
      firstWeight: '首重(克)',
      firstCharge: '首费(元)',
      continuousWeight: '续重(克)',
      renewals: '续费(元)',
      fullPackage: '满额包邮(元)',
      type: '类型',
      nonDistributionArea: '不配送区域',
      distributionArea: '只配送区域',
      selectionArea: '选择区域',
      search: {
        keyWords: '关键词'
      },
      tableHead: {
           id:'ID',
           storeId:'店铺ID',
           storeName:'店铺名称',
           name:'运费模板名称',
           type:'类型,0:按件,1:按重量计费',
           order:'排序',
           first:'默认首重/首件',
           second:'默认续重/续件',
           firstPrice:'默认首重/首件价格',
           secondPrice:'默认续重/续件价格',
           freeShipping:'满多少包邮',
           enabled:'是否可用',
           isDefault:'是否默认',
           specialAreas:'不配送的特殊区域',
           specialAreasType:'0代表不配送区域,1代表只配送',
      }
    }
  },
  en: {
    freightTemplate: {
      sort: 'sort',
      freightTempName: 'freight template name',
      isDefault: 'is Default',
      yes: 'yes',
      no: 'no',
      availability: 'availability',
      billingMode: 'billing mode',
      billing: 'billing',
      chargingByWeight: 'charging by weight',
      defaultFreight: 'default freight',
      firstWeight: 'first weight (gram)',
      firstCharge: 'First charge (yuan)',
      continuousWeight: 'Continuous weight (g)',
      renewals: 'Renewals (yuan)',
      type: 'type',
      fullPackage: 'Full package (RMB)',
      nonDistributionArea: 'non-distribution area',
      distributionArea: 'distribution area only',
      selectionArea: 'selection area',
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
       //TODO 需要翻译成英文
         id:'ID',
         storeId:'店铺ID',
         storeName:'店铺名称',
         name:'运费模板名称',
         type:'类型,0:按件,1:按重量计费',
         order:'排序',
         first:'默认首重/首件',
         second:'默认续重/续件',
         firstPrice:'默认首重/首件价格',
         secondPrice:'默认续重/续件价格',
         freeShipping:'满多少包邮',
         enabled:'是否可用',
         isDefault:'是否默认',
         specialAreas:'不配送的特殊区域',
         specialAreasType:'0代表不配送区域,1代表只配送',
      }
    }
  },
  ko: {
    freightTemplate: {
      sort: '정렬',
      freightTempName: '운임 템플릿 이름',
      isDefault: '묵인 여부',
      yes: '예',
      no: '안',
      availability: '사용할 수 있는지 여부',
      billingMode: '요금 계산 방식',
      billing: '수수료',
      chargingByWeight: '중량에 따라 비용을 계산하다.',
      defaultFreight: '기본 운임',
      firstWeight: '첫 중량 (그램)',
      firstCharge: '첫 요금 (원)',
      continuousWeight: '중량 (그램)',
      renewals: '재비 (원)',
      fullPackage: '만액 소포 (원)',
      type: '유형',
      nonDistributionArea: '배송 영역',
      distributionArea: '배송 영역만',
      selectionArea: '선택 영역',
      search: {
        keyWords: '키워드'
      },
      tableHead: {
       //TODO 需要翻译成韩文
         id:'ID',
         storeId:'店铺ID',
         storeName:'店铺名称',
         name:'运费模板名称',
         type:'类型,0:按件,1:按重量计费',
         order:'排序',
         first:'默认首重/首件',
         second:'默认续重/续件',
         firstPrice:'默认首重/首件价格',
         secondPrice:'默认续重/续件价格',
         freeShipping:'满多少包邮',
         enabled:'是否可用',
         isDefault:'是否默认',
         specialAreas:'不配送的特殊区域',
         specialAreasType:'0代表不配送区域,1代表只配送',
      }
    }
  }
}
