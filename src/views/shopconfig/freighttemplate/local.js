export default {
  zh: {
    freightTemplate: {
      search: {
        keyWords: '关键词'
      },
      tableHead: {
           id:'ID',
           storeId:'店铺ID',
           storeName:'店铺名称',
           name:'运费模板名称',
           type:'类型,0:按件,1:按重量计费',
           order:'排序',
           first:'默认首重/首件',
           second:'默认续重/续件',
           firstPrice:'默认首重/首件价格',
           secondPrice:'默认续重/续件价格',
           freeShipping:'满多少包邮',
           enabled:'是否可用',
           isDefault:'是否默认',
           specialAreas:'不配送的特殊区域',
           specialAreasType:'0代表不配送区域,1代表只配送',
      }
    }
  },
  en: {
    freightTemplate: {
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
       //TODO 需要翻译成英文
         id:'ID',
         storeId:'店铺ID',
         storeName:'店铺名称',
         name:'运费模板名称',
         type:'类型,0:按件,1:按重量计费',
         order:'排序',
         first:'默认首重/首件',
         second:'默认续重/续件',
         firstPrice:'默认首重/首件价格',
         secondPrice:'默认续重/续件价格',
         freeShipping:'满多少包邮',
         enabled:'是否可用',
         isDefault:'是否默认',
         specialAreas:'不配送的特殊区域',
         specialAreasType:'0代表不配送区域,1代表只配送',
      }
    }
  },
  ko: {
    freightTemplate: {
      search: {
        keyWords: '키워드'
      },
      tableHead: {
       //TODO 需要翻译成韩文
         id:'ID',
         storeId:'店铺ID',
         storeName:'店铺名称',
         name:'运费模板名称',
         type:'类型,0:按件,1:按重量计费',
         order:'排序',
         first:'默认首重/首件',
         second:'默认续重/续件',
         firstPrice:'默认首重/首件价格',
         secondPrice:'默认续重/续件价格',
         freeShipping:'满多少包邮',
         enabled:'是否可用',
         isDefault:'是否默认',
         specialAreas:'不配送的特殊区域',
         specialAreasType:'0代表不配送区域,1代表只配送',
      }
    }
  }
}
