// 定制表头显示的数据，prop需要与表格数据对应
const tableHeaderData = {
    hasSelection: false, //是否可以多选
    data: [
        { label: 'ID', prop: 'id' },
        { label: '店铺ID', prop: 'storeId' },
        { label: '店铺名称', prop: 'storeName' },
        { label: '运费模板名称', prop: 'name' },
        { label: '类型,0:按件,1:按重量计费', prop: 'type' },
        { label: '排序', prop: 'order' },
        { label: '默认首重/首件', prop: 'first' },
        { label: '默认续重/续件', prop: 'second' },
        { label: '默认首重/首件价格', prop: 'firstPrice' },
        { label: '默认续重/续件价格', prop: 'secondPrice' },
        { label: '满多少包邮', prop: 'freeShipping' },
        { label: '是否可用', prop: 'enabled' },
        { label: '是否默认', prop: 'isDefault' },
        { label: '不配送的特殊区域', prop: 'specialAreas' },
        { label: '0代表不配送区域,1代表只配送', prop: 'specialAreasType' },
    ]
}


const searchData = [
  { desc: '关键词', type: 'input', key: 'keyWords' },
  //{ desc: '时间选择', type: 'dateTime', key: 'dateTime' },
  //{ desc: '状态', type: 'select', key: 'isShow', options: [{ value: '1', label: '是' }, { value: '0', label: '否' }] }
]

const tableData = []

const name = 'freightTemplate'

export {
  tableHeaderData, // 表头配置
  searchData, // 查询配置
  tableData, // 表格数据
  name // 文件名，为国际化用
}