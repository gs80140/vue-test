export default {
  zh: {
    store: {
      search: {
        keyWords: '关键词'
      },
      tableHead: {
           id:'店铺ID',
           storeName:'店铺名称',
           companyName:'公司名',
           companyProvince:'所在省',
           companyCity:'所在市',
           companyRegion:'所在区',
           companyAddress:'公司地址',
           latitude:'纬度',
           longitude:'经度',
           sellerId:'卖家ID',
           sellerAccount:'卖家主账号',
           sellerPassword: '登陆密码',
           addTime:'添加时间',
           updateTime:'更新时间',
      }
    }
  },
  en: {
    store: {
      search: {
        keyWords: 'keyWords'
      },
      tableHead: {
       //TODO 需要翻译成英文
         id:'店铺ID',
         storeName:'店铺名称',
         companyName:'公司名',
         companyProvince:'所在省',
         companyCity:'所在市',
         companyRegion:'所在区',
         companyAddress:'公司地址',
         latitude:'纬度',
         longitude:'经度',
         sellerId:'卖家ID',
         sellerAccount:'卖家主账号',
         sellerPassword: '登陆密码',
         addTime:'添加时间',
         updateTime:'更新时间',
      }
    }
  },
  ko: {
    store: {
      search: {
        keyWords: '키워드'
      },
      tableHead: {
       //TODO 需要翻译成韩文
         id:'店铺ID',
         storeName:'店铺名称',
         companyName:'公司名',
         companyProvince:'所在省',
         companyCity:'所在市',
         companyRegion:'所在区',
         companyAddress:'公司地址',
         latitude:'纬度',
         longitude:'经度',
         sellerId:'卖家ID',
         sellerAccount:'卖家主账号',
         sellerPassword: '登陆密码',
         addTime:'添加时间',
         updateTime:'更新时间',
      }
    }
  }
}
