// 定制表头显示的数据，prop需要与表格数据对应
const tableHeaderData = {
    hasSelection: false, //是否可以多选
    data: [
        { label: '店铺ID', prop: 'id' },
        { label: '店铺名称', prop: 'storeName' },
        { label: '公司名', prop: 'companyName' },
        { label: '所在省', prop: 'companyProvince' },
        { label: '所在市', prop: 'companyCity' },
        { label: '所在区', prop: 'companyRegion' },
        { label: '公司地址', prop: 'companyAddress' },
        { label: '纬度', prop: 'latitude' },
        { label: '经度', prop: 'longitude' },
        { label: '卖家ID', prop: 'sellerId' },
        { label: '卖家主账号', prop: 'sellerAccount' },
        { label: '添加时间', prop: 'addTime' },
        { label: '更新时间', prop: 'updateTime' },
    ],
  add: [
    { label: '店铺名称', prop: 'storeName' },
    { label: '公司名', prop: 'companyName' },
    { label: '所在省', prop: 'companyProvince' },
    { label: '所在市', prop: 'companyCity' },
    { label: '所在区', prop: 'companyRegion' },
    { label: '公司地址', prop: 'companyAddress' },
    { label: '纬度', prop: 'latitude' },
    { label: '经度', prop: 'longitude' },
    { label: '卖家主账号', prop: 'sellerAccount' },
    { label: '卖家密码', prop: 'sellerPassword' },
  ]
}


const searchData = [
  { desc: '关键词', type: 'input', key: 'keyWords' },
  //{ desc: '时间选择', type: 'dateTime', key: 'dateTime' },
  //{ desc: '状态', type: 'select', key: 'isShow', options: [{ value: '1', label: '是' }, { value: '0', label: '否' }] }
]

const tableData = []

const name = 'store'

export {
  tableHeaderData, // 表头配置
  searchData, // 查询配置
  tableData, // 表格数据
  name // 文件名，为国际化用
}
