// 定制表头显示的数据，prop需要与表格数据对应
export const tableHeaderData = {
    hasSelection: false, //是否可以多选
    data: [
        { label: '主键id', prop: 'id' },
        { label: '父id', prop: 'pid' },
        { label: '组织名称', prop: 'name' },
        { label: '排序,小的在前面', prop: 'sortNum' },
        { label: '备注', prop: 'remark' },
    ]
}

export const tableData = []